#ifndef CANDIDATOS_HPP
#define CANDIDATOS_HPP

#include <string>

class Candidatos
{
    private:
        std::string cargo;
        std::string num_voto_candidato;
        std::string nome_candidato;
        std::string apelido_candidato;
        std::string sigla_partido;
        std::string nome_partido;
        int quantidade_de_votos;

    public:
        Candidatos();

        /* Esses atributos são os dados do arquivo CSV */
        Candidatos(std::string cargo, std::string num_voto_candidato,
        std::string nome_candidato, std::string apelido_candidato,
        std::string sigla_partido, std::string nome_partido);

        ~Candidatos();

        void set_cargo(std::string cargo);

        void set_num_voto_candidato(std::string num_voto_candidato);

        void set_nome_candidato(std::string nome_candidato);

        void set_apelido_candidato(std::string apelido_candidato);

        void set_sigla_partido(std::string sigla_partido);

        void set_nome_partido(std::string nome_partido);

        std::string get_cargo();

        std::string get_num_voto_candidato();

        std::string get_nome_candidato();

        std::string get_apelido_candidato();

        std::string get_sigla_partido();

        std::string get_nome_partido();

        /* Método para acrescentar 1 voto no candidato */
        void add_voto();

        /* Método que retorna a qntd de votos no final da eleição */
        int get_quant_votos();
};

#endif