#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include <string>
#include <iostream>

class Eleitor
{
    private:
        long int cpf;
        long int titulo_eleitor;
        std::string nome;
        std::string data_nascimento;

    public:
        Eleitor();
        ~Eleitor();

        void set_cpf();
        void set_nome();
        void set_titulo_eleitor();
        void set_data_nascimento();
        
        long int get_cpf();
        long int get_titulo_eleitor();
        std::string get_data_nascimento();
        std::string get_nome();

        void cadastrar_eleitor();
};

#endif