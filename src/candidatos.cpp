#include "candidatos.hpp"

using namespace std;

Candidatos::Candidatos()
{
    this->cargo = "null";
    this->num_voto_candidato = "";
    this->nome_candidato = "";
    this->apelido_candidato = "";
    this->sigla_partido = "";
    this->nome_partido = "";
    quantidade_de_votos = 0;
}

Candidatos::Candidatos(string cargo, string num_voto_candidato,
    string nome_candidato, string apelido_candidato,
    string sigla_partido, string nome_partido)
{
    this->cargo = cargo;
    this->num_voto_candidato = num_voto_candidato;
    this->nome_candidato = nome_candidato;
    this->apelido_candidato = apelido_candidato;
    this->sigla_partido = sigla_partido;
    this->nome_partido = nome_partido;
    
    quantidade_de_votos = 0;
}

Candidatos::~Candidatos()
{
    quantidade_de_votos = 0;
}

void Candidatos::set_cargo(string cargo)
{
    this->cargo = cargo;
}

void Candidatos::set_num_voto_candidato(string num_voto_candidato)
{
    this->num_voto_candidato = num_voto_candidato;
}

void Candidatos::set_nome_candidato(string nome_candidato)
{
    this->nome_candidato = nome_candidato;
}

void Candidatos::set_apelido_candidato(string apelido_candidato)
{
    this->apelido_candidato = apelido_candidato;
}

void Candidatos::set_sigla_partido(string sigla_partido)
{
    this->sigla_partido = sigla_partido;
}

void Candidatos::set_nome_partido(string nome_partido)
{
    this->nome_partido = nome_partido;
}

string Candidatos::get_cargo()
{
    return cargo;
}

string Candidatos::get_num_voto_candidato()
{
    return num_voto_candidato;
}

string Candidatos::get_nome_candidato()
{
    return nome_candidato;
}

string Candidatos::get_apelido_candidato()
{
    return apelido_candidato;
}

string Candidatos::get_sigla_partido()
{
    return sigla_partido;
}

string Candidatos::get_nome_partido()
{
    return nome_partido;
}

void Candidatos::add_voto()
{
    quantidade_de_votos++;
}

int Candidatos::get_quant_votos()
{
    return quantidade_de_votos;
}