#include "eleitor.hpp"

using namespace std;

/* Nada é inicializado no construtor, somente é alocado a memória */
Eleitor::Eleitor() { }

Eleitor::~Eleitor() { }

long int Eleitor::get_cpf() { return cpf; }

long int Eleitor::get_titulo_eleitor() { return titulo_eleitor; }

string Eleitor::get_data_nascimento() { return data_nascimento; }

string Eleitor::get_nome() { return nome; }



/* Método para cadastrar os atributos do eleitor pelo terminal */
void Eleitor::cadastrar_eleitor()
{
    set_cpf();
    set_titulo_eleitor();
    set_data_nascimento();
    set_nome();
}



/* Método para receber o cpf do terminal */
void Eleitor::set_cpf()
{
    /* Limpar terminal */
    cout << string(50, '\n');

    cout << "Digite seu CPF: (Somente números)" << endl;

    long cpf_inserido;

    /* Enquanto o cpf inserido ñ for válido */
    /* < 100000000, caso seja digitado '.' ou '-' */
    while(!(cin>>cpf_inserido))
    {
        /* Limpo a tela e limpo o buffer 'cin' */
        cout << string(50, '\n');
        cin.clear();
        cin.ignore();

        cout << "Digite somente números! Não use pontos! Não use espaços!\n" << endl;

        cout << "Digite seu CPF: (Somente números)" << endl;
    }

    this->cpf = cpf_inserido;

    /* Limpo a tela e limpo o buffer 'cin' */
    cout << string(50, '\n');
    cin.clear();
    cin.ignore();
}



/* Método para receber o título de eleitor pelo terminal */
void Eleitor::set_titulo_eleitor()
{
    cout << string(50, '\n');

    cout << "Digite o seu Título de Eleitor: (Somente números)" << endl;

    long titulo_inserido;

    /* Enquanto o título de eleitor não for válido */
    while(!(cin>>titulo_inserido))
    {
        /* Limpo a tela e limpo o buffer 'cin' */
        cout << string(50, '\n');
        cin.clear();
        cin.ignore();

        cout << "Digite somente números! Não use pontos! Não use espaços!" << endl;

        cout << "Digite o seu Título de Eleitor: (Somente números)" << endl;
    }

    this->titulo_eleitor = titulo_inserido;
    
    /* Limpo a tela e limpo o buffer 'cin' */
    cout << string(50, '\n');
    cin.clear();
    cin.ignore();
}



/* Método para receber a data de nascimento via terminal */
void Eleitor::set_data_nascimento()
{
    /* Limpar a tela */
    cout << string(50, '\n');

    cout << "Digite sua data de nascimento: (ex.: 12/01/1999)" << endl;

    string data_inserida;

    /* Enquanto o nome inserido não for válido */
    while(!(cin>>data_inserida))
    {
        /* Limpo a tela e limpo o buffer 'cin' */
        cout << string(50, '\n');
        cin.clear();
        cin.ignore();

        cout << "Digite somente os números e as barras! Não use pontos! Não use espaços!" << endl;

        cout << "Digite sua data de nascimento: (ex.: 12/01/1999)" << endl;
    }

    this->data_nascimento = data_inserida;

    /* Limpo a tela e limpo o buffer 'cin' */
    cout << string(50, '\n');
    cin.clear();
    cin.ignore();
}



/* Método para receber o nome via terminal */
void Eleitor::set_nome()
{
    /* Limpar a tela */
    cout << string(50, '\n');

    cout << "Digite seu nome completo:" << endl;

    string nome_inserido;

    getline(cin, nome_inserido);

    this->nome = nome_inserido;
}