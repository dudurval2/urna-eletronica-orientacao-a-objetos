#include "urna.hpp"
#include "CSVReader.hpp"

using namespace std;

/* Construtor padrão*/
Urna::Urna() 
{
    contagem_de_eleitores = 0;
    max_eleitores = 0;
    votacao = false;
}

/* Destrutor padrão */
Urna::~Urna() {}

/* Método que inicializar o funcionamento da urna */
bool Urna::init()
{
    /* Variável para verificar ocorrencia de erros */
    bool erros;

    /* Método da urna que interage com a Classe CSV */
    erros = CSV_to_urna();
    if(erros) return 1;

    /* Inicializa a urna com contagem zero e configura o máximo de eleitores possíveis */
    set_max_eleitores();

    working();

    print_log();

    resultado_das_eleicoes();

    return 0;
}

/* Método da urna que interage com a Classe CSV */
bool Urna::CSV_to_urna()
{
    CSVReader outros_candidatos("candidatos.csv", ';');
    CSVReader presidentes("presidentes.csv", ';');

    bool erros;
    erros = presidentes.load_candidatos(this);
    if(erros) return 1;

    erros = outros_candidatos.load_candidatos(this);
    if(erros) return 1;

    return 0;
}

/* Método para inicializar a urna, com o máximo de eleitores na urna e contagem igual a zero */
void Urna::set_max_eleitores()
{
    /* Limpar a tela */
    cout << string(100, '\n');

    cout << "Digite o máximo de eleitores que podem votar nesta urna:" << endl;

    int maximo_eleitores = 0;

    while(!(cin>>maximo_eleitores))
    {
        /*Limpa todos os erros e o buffer do teclado */
        cin.clear();
        cin.ignore();

        /* Limpar a tela */
        cout << string(100, '\n');

        cout << "Valor inválido!!\n" << endl;
        cout << "Digite Novamente:\n"<< endl;
        cout << "Digite o máximo de eleitores que podem votar nesta urna:" << endl;
    }

    /* Limpar a tela */
    cout << string(100, '\n');

    this->max_eleitores = maximo_eleitores;

    /* Urna inicializada sem voto de eleitores */
    contagem_de_eleitores = 0;

    /* Bool indica a votação começou */
    votacao = true;
}

/* Loop de funcionamento da urna */
void Urna::working()
{
    /* Enquanto contagem válida e votação == true */
    while(contagem_de_eleitores < max_eleitores and votacao)
    {
        /* Objeto da classe eleitor que irá votar */
        Eleitor eleitor;

        /* Método para cadastrar os dados do eleitor */
        eleitor.cadastrar_eleitor();

        /* Escreve o candidato no log da urna */
        log_eleitor(&eleitor);

        /* Método para receber os votos do candidato */
        recebe_voto();

        if(stop_votacao())
        {
            votacao = false;
        }
    }

}

/* Escreve as informações do eleitor no log da urna */
void Urna::log_eleitor(Eleitor * eleitor)
{
    ofstream file;
    file.open("log.txt", ios_base::app);

    file << "NOME: " << (*eleitor).get_nome() << endl;

    /* Para casos especiais em que o cpf começa com zeros, é preciso acrescentar os zeros que são ignorados na hora de scanear um long int */
    long cpf_11_digitos = (*eleitor).get_cpf();
    
    file << "CPF: " << setw(11) << setfill('0') << cpf_11_digitos << endl;

    file << "Data de Nascimento: " << (*eleitor).get_data_nascimento() << endl;

    /* Para casos especiais em que o titulo de eleitor começa com zeros, é preciso acrescentar os zeros que são ignorados na hora de scanear um long int */
    long int titulo_12_digitos = (*eleitor).get_titulo_eleitor();

    file << "Titulo de Eleitor: " << setw(12) << setfill('0') << titulo_12_digitos << endl;

    file.close();
}

void Urna::recebe_voto()
{
    vector <string> cargos;

    cargos.push_back("DEPUTADO FEDERAL"); cargos.push_back("DEPUTADO DISTRITAL"); cargos.push_back("SENADOR - 1ª VAGA"); cargos.push_back("SENADOR - 2ª VAGA"); cargos.push_back("GOVERNADOR"); cargos.push_back("PRESIDENTE");

    Candidatos * cand_votado = nullptr;

    for(int i=0; i<6; i++)
    {
        bool confirmar = false;
        int numero_votado;

        while(!confirmar)
        {
            /* Limpar a tela */
            cout << string(100, '\n');

            cout << cargos[i] << endl << endl;
            
            cout << "Voto NULO dígito   [0]" << endl;
            cout << "Voto BRANCO dígito [1]\n" << endl;

            cout << "\nDigite o número do seu voto: " << endl;

            cin >> numero_votado;

            /* Limpar o buffer e remover os erros se tiver */
            cin.ignore();
            cin.clear();

            if(numero_votado == 0)
            {
                cout << string(100, '\n');
                cout << "Voto NULO\n" << endl;
            }

            else if(numero_votado == 1)
            {
                cout << string(100, '\n');
                cout << "Voto BRANCO" << endl;
            }

            else
            {
                cout << string(100, '\n');

                cand_votado = procura_candidato(numero_votado,i);
            }

            if(cand_votado == nullptr)
            {
                cout << "Candidato inválido" << endl;
                cout << "Voto contabilizado como NULO" << endl;
            }

            cout << "Deseja confirmar voto?\n" << endl;
            cout << "Confirmar voto (1)" << endl;
            cout << "Corrigir  voto (0)" << endl;

            while(!(cin >> confirmar))
            {
                /* limpa o buffer */
                cin.clear();
                cin.ignore();

                cout << "Deseja confirmar voto?" << endl;
                cout << "Confirmar voto (1)" << endl;
                cout << "Corrigir  voto (0)" << endl << endl;
            }
        }

        /* Adiciona um voto na contagem de votos do candidato */

        /* Se tiver sido votado em algum candidato */
        if(cand_votado) (*cand_votado).add_voto();

        log_voto(numero_votado, cargos[i]);

        add_contagem_de_eleitores();
    }
}

Candidatos* Urna::procura_candidato(int numero_voto, int pos_loop)
{
    /* Objeto retornado */
    Candidatos * candidato = nullptr;

    /* CASO 0 & CASO 1 -> DEPUTADO FEDERAL E DISTRITAL*/
    if(pos_loop<2)
    {
        Candidatos * deputado;

        deputado = confere_numero(numero_voto, pos_loop);

        /* Se deputado não for um ponteiro nulo */
        if(deputado)
        {
            /* Referencia para adicionar voto após confirmar */
            candidato = deputado;

            imprimi_deputado_votado(deputado);
        }
    }

    /* CASO 2 & CASO 3 -> Senadores (1 e 2 Vaga) */
    else if(pos_loop<4)
    {
        Candidatos *senador, *suplente1, *suplente2;

        string cargo = "SENADOR";
        senador = confere_numero(numero_voto, pos_loop, cargo);

        cargo = "SUPLENTE1";
        suplente1 = confere_numero(numero_voto, pos_loop, cargo);

        cargo = "SUPLENTE2";
        suplente2 = confere_numero(numero_voto, pos_loop, cargo);

        /* Se deputado não for um ponteiro nulo */
        if(senador)
        {
            /* Referencia para adicionar voto após confirmar */
            candidato = senador;

            imprimi_deputado_votado(senador);
            imprimi_deputado_votado(suplente1);
            imprimi_deputado_votado(suplente2);
        }
    }

    else
    {
        Candidatos *cand, *vice;

        string cargo = "CANDIDATO";
        cand = confere_numero(numero_voto, pos_loop, cargo);

        cargo = "VICE-CANDIDATO";
        vice = confere_numero(numero_voto, pos_loop, cargo);

        if(cand)
        {
            candidato = cand;

            imprimi_deputado_votado(cand);
            imprimi_deputado_votado(vice);
        }
    }

    return candidato;
}

void Urna::imprimi_deputado_votado(Candidatos * candidato)
{
    cout << candidato->get_cargo();

    cout << "\nNome do candidato: " << candidato->get_apelido_candidato() << endl;

    cout << "Partido do candidato: " << candidato->get_sigla_partido() << endl << endl;
}

/* Método para encerrar as eleições antes de atingir o máximo de eleitores possíveis */
bool Urna::stop_votacao()
{
    /* Limpar a tela */
    cout << string(100, '\n');

    cout << "Continuar a votação? (sim - 1) (não - 0)" << endl;

    bool still_eleicoes;


    while(!(cin >> still_eleicoes))
    {
        cout << "Continuar a votação? (sim - 1) (não - 0)" << endl;
    }

    if(still_eleicoes) return false;

    return true;
}

/* Método para adcionar um candidato ao seu respectivo vector na urna de acordo com ao cargo disputado */
void Urna::adc_candidato(Candidatos cand)
{
    if(cand.get_cargo() == "PRESIDENTE" or cand.get_cargo() == "VICE-PRESIDENTE")
    {
        vec_presidentes.push_back(cand);
    }

    else if(cand.get_cargo() == "GOVERNADOR" or cand.get_cargo() == "VICE-GOVERNADOR")
    {
        vec_governadores.push_back(cand);
    }

    else if(cand.get_cargo() == "DEPUTADO DISTRITAL")
    {
        vec_deputados_distritais.push_back(cand);
    }

    else if(cand.get_cargo() == "DEPUTADO FEDERAL")
    {
        vec_deputados_federais.push_back(cand);
    }

    else
    {
        vec_senadores.push_back(cand);
    }
}

/* Método para acrescentar 1 ao total de eleitores que votaram */
void Urna::add_contagem_de_eleitores()
{
    contagem_de_eleitores++;
}

/* Método para escrever no log da urna o cargo e o voto do eleitor */
void Urna::log_voto(int numero_votado, string cargo)
{
    
    ofstream file;
    file.open("log.txt", ios_base::app);

    /* O número zero é reservado para voto NULO */
    if(numero_votado!=0)
    {
        file << "\tVoto p/" << cargo << ": " << numero_votado<<endl;
    }

    else
    {
        file << "\tVoto p/" << cargo << ": " << "NULO" << endl;
    }

    /* O voto para o presidente é o ultimo do loop de votação, assim é preciso de uma quebra de linha para separar as informações do voto do próximo eleitor  */
    if(cargo == "PRESIDENTE")
    {
        cout << endl << endl;
    }

    file.close();
}

/* Método para ler o log da urna e escrever no terminal */
void Urna::print_log()
{
    cout << string(100, '\n');
    cout << "Log da votação:\n" << endl;

    fstream file;
    file.open("log.txt", fstream::in);

    string linha_toda;
    while(getline(file, linha_toda))
    {
        cout << linha_toda << endl;
    }
}

/* Método para percorrer todos os vetores da urna e retornar os candidatos que tiverem mais votos */
void Urna::resultado_das_eleicoes()
{
    string nome_senador_1;

    cout << "Aperte qualquer tecla para receber os resultados:" << endl;

    string pause; cin >> pause;

    /* Limpar a tela */
    cout << string(100, '\n');

    vector<string> cargos;

    cargos.push_back("DEPUTADO FEDERAL"); cargos.push_back("DEPUTADO DISTRITAL"); cargos.push_back("SENADOR - 1ª VAGA"); cargos.push_back("SENADOR - 2ª VAGA"); cargos.push_back("GOVERNADOR"); cargos.push_back("PRESIDENTE");

    /* Ponteiro de vector para apontar para um vector da urna de acordo com o cargo procurado */
    vector <Candidatos> * vector_candidatos;

    cout << "Resultado das eleições:" << endl << endl;

    for (int i=0; i<6; i++)
    {
        if(i==0)
            vector_candidatos = &vec_deputados_federais;

        else if(i==1)
            vector_candidatos = &vec_deputados_distritais;
        
        else if(i==2 or i==3)
            vector_candidatos = &vec_senadores;

        else if(i==4)
            vector_candidatos = &vec_governadores;
        
        else vector_candidatos = &vec_presidentes;

        
        Candidatos * vencedor = nullptr;

        int quantidade_de_votos=0;

        /* Estou usando o tipo 'auto' para melhorar a legibilidade, para não ter que escrever tudo isso no for vector<Candidatos>::iterator it = vector_candidatos->begin(); */
        for (auto it = vector_candidatos->begin(); it != vector_candidatos->end(); it++)
        {
            if(it->get_quant_votos() > quantidade_de_votos)
            {
                if(i==2)
                {
                    nome_senador_1 = it->get_apelido_candidato();
                }

                if(i!=3)
                {
                    vencedor = &(*it);
                }

                else
                {
                    if(it->get_apelido_candidato() != nome_senador_1)
                    {
                        vencedor = &(*it);
                    }
                }

                
            }
        }

        cout << "Cargo Disputado " << cargos[i] << endl;

        if(vencedor)
        {
            cout << "Nome do candidato vencedor: " << vencedor->get_apelido_candidato() << endl;

            cout << "Partido do Candidato: " << vencedor->get_sigla_partido() << endl;

            cout << "Quantidade de votos: " << vencedor->get_quant_votos() << endl<<endl;
        }

        else
        {
            cout << "Nome do candidato vencedor: " << "NENHUM CANDIDATO COM VOTO" << endl << endl;
        }
    }
}

/* Método usado para conferir se o número recebido é de algum dos candidatos que estão participando da eleição. Caso seja de algum candidato, é retornado o endereço, caso não seka, é retornado o endereço de um candidato nulo */
Candidatos* Urna::confere_numero(int numero_votado, int aux_loop, string dica_cargo)
{
    /* Essa variável auxilia para diferenciar os cargos que estão nos mesmo vetores, como vices e suplentes */
    string cargo_procurado;
    
    /* Esse iterator irá receber o primeiro objeto de um vector de acordo com o cargo procurado, como irei usá-lo para percorrer  vector precisei declará-lo antes */
    vector<Candidatos>::iterator it;
    
    /* Utilizo esse ponteiro de vector para:  Primeiro receber o endereço de um vector de acordo com o cargo procurado. Depois utilizá-lo para condição de parada do loop for */
    vector <Candidatos> * vector_candidatos;

    /* A variável aux_loop indica em qual vector da urna irei procurar */

    /* Caso 0 - Deputado Distrital */
    if(aux_loop==0)
    {
        vector_candidatos = &vec_deputados_federais;
        it = vector_candidatos->begin();
        cargo_procurado = "DEPUTADO FEDERAL";
    }

    /* Caso 1 - Deputado Distrital */
    else if(aux_loop==1)
    {
        vector_candidatos = &vec_deputados_distritais;
        it=vector_candidatos->begin();
        cargo_procurado = "DEPUTADO DISTRITAL";
    }

    /* Caso 2 e 3 - Senador 1ª Vaga e 2ª Vaga */
    else if(aux_loop==2 or aux_loop ==3)
    {
        vector_candidatos = &vec_senadores;

        it=vector_candidatos->begin();

        if(dica_cargo == "SENADOR" ) 
        {
            cargo_procurado = "SENADOR";
        }

        else if(dica_cargo == "SUPLENTE1")
        {
            cargo_procurado = "1� SUPLENTE";
        }

        else { cargo_procurado = "2� SUPLENTE"; }

    }

    /* Caso 4 - Governador */
    else if(aux_loop==4)
    {
        vector_candidatos = &vec_governadores;
        it=vector_candidatos->begin();

        if(dica_cargo == "CANDIDATO" )
        {
            cargo_procurado = "GOVERNADOR";
        }

        else { cargo_procurado = "VICE-GOVERNADOR"; }
    }

    /* Caso 5 - Presidente */
    else
    {
        vector_candidatos = &vec_presidentes;
        it=vector_candidatos->begin();

        if(dica_cargo == "CANDIDATO" )
        {
            cargo_procurado = "PRESIDENTE";
        }

        else
        {
            cargo_procurado = "VICE-PRESIDENTE";
        }
    }

    /* Após determinar em qual vector da urna irei procurar o candidato, busco em todas as posições */
    for (; it != vector_candidatos->end() ; it++)
    {
        /* Se o cargo e o número votado do iterator for o mesmo que procuro, o endereço do interator é retornado. 
        É preciso comparar tanto o cargo quanto o número pq vices e suplentes tem o mesmo número do candidato principal */
        if ( it->get_cargo() == cargo_procurado
        and it->get_num_voto_candidato() == to_string(numero_votado))
        {
            /* Referencia para o candidato achado */
            return &(*it);
        }
    }

    /* Caso o candidato não exista, é retornado esse ponteiro*/
    Candidatos * null_cand = nullptr;

    return null_cand;
}